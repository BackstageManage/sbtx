package com.haifeng.sbtx.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * <p>
 *  通用返回类
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-11-28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Rest<T> implements Serializable {

    @ApiModelProperty(value = "是否成功:true-成功；false-失败")
    private Boolean success;
    @ApiModelProperty(value = "返回对象")
    private T data;
    @ApiModelProperty(value = "错误编码")
    private Integer code;
    @ApiModelProperty(value = "错误信息")
    private String message;

    /**
     * 成功返回(默认)
     * @param data
     * @return
     */
    public static Rest success(Object data){
        return success(data, HttpStatus.OK.value(), ErrorCode.SUCCESS_MSG);
    }

    /**
     * 成功返回
     * @param data
     * @param code
     * @return
     */
    public static Rest success(Object data,Integer code,String message){
        return new Rest(true,data,code,message);
    }


    /**
     * 失败返回
     * @param code
     * @param message
     * @return
     */
    public static Rest error(Integer code,String message){
        return error(null,code,message);
    }


    /**
     * 失败返回
     * @param code
     * @param message
     * @return
     */
    public static Rest error(Object data,Integer code,String message){
        return new Rest(false,data,code,message);
    }

}
