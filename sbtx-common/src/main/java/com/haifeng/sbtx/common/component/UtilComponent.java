package com.haifeng.sbtx.common.component;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

/**
 * <p>
 *  AntPathMatcher
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-18
 */
@Component
public class UtilComponent {

    @Bean
    public AntPathMatcher antPathMatcher(){
        return new AntPathMatcher();
    }
}
