package com.haifeng.sbtx.common.util;


import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class CryptoUtil {

    /**
     * 加密
     *
     * @param str
     * @return
     */
    public static String encoder(String str) {
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] textByte = str.getBytes(StandardCharsets.UTF_8);
        return encoder.encodeToString(textByte);
    }

    /**
     * 解密
     *
     * @param encodedStr
     * @return
     */
    public static String decode(String encodedStr) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedStr), StandardCharsets.UTF_8);
    }
}
