package com.haifeng.sbtx.common.base;

import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * <p>
 *  FASTJSON序列化
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-05
 */
public class JsonSerializer {


    public static SerializerFeature[] serializerFeatures = new SerializerFeature[] {
            SerializerFeature.PrettyFormat,
            SerializerFeature.SortField,
            SerializerFeature.WriteMapNullValue
    };
}
