package com.haifeng.sbtx.common.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 *  自定义业务异常类
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-11-28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BussinessException extends RuntimeException {

    private Integer errCode;

    private static final long serialVersionUID = 1L;
}
