package com.haifeng.sbtx.common.base;

/**
 * <p>
 *  错误码
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-11-28
 */
public class ErrorCode {

    /**
     *  成功
     */
    public static final String SUCCESS_MSG = "success";

    /**
     *  token不正确
     */
    public static final Integer INVALID_TOKEN = 400001;

    /**
     *  访问资源需要身份验证
     */
    public static final Integer NEED_AUTH = 400002;

    /**
     *  refresh_token不正确
     */
    public static final Integer INVALID_REFRESH_TOKEN = 400003;

    /**
     *  授权码不正确
     */
    public static final Integer INVALID_AUTHORIZATION_CODE = 400004;

    /**
     *  密码不正确
     */
    public static final Integer BAD_CREDENTIALS = 400005;

    /**
     *  无效的Token
     */
    public static final Integer TOKEN_WAS_NOT_RECOGNISED = 400006;

    /**
     *  服务发生错误,请重试
     */
    public static final Integer SERVER_ERROR = 400007;

    /**
     *  scope不正确
     */
    public static final Integer INVALID_SCOPE = 400008;
    /**
     *  不支持的授权方式
     */
    public static final Integer UNSUPPORTED_GRANT_TYPE = 400009;
    /**
     *  缺少授权方式grant_type
     */
    public static final Integer MISSING_GRANT_TYPE = 400010;
    /**
     *  缺少授权码code
     */
    public static final Integer AN_AUTHORIZATION_CODE_MUST_BE_SUPPLIED = 400011;
    /**
     *  没有权限访问此资源
     */
    public static final Integer NO_PERMISSION = 400012;



    /**
     *  参数校验未通过异常@RequestParam
     */
    public static final Integer ERROR_PARAMS_NOT_VALID = 500001;
    /**
     *  参数校验未通过异常@RequestBody
     */
    public static final Integer ERROR_JSON_NOT_VALID = 500002;



}
