package com.haifeng.sbtx.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.sbtx.mapper.entity.UserRole;

/**
 * <p>
 * 用户角色关联表 服务类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface UserRoleService extends IService<UserRole> {

}
