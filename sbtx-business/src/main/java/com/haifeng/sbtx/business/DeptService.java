package com.haifeng.sbtx.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.sbtx.mapper.entity.Dept;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface DeptService extends IService<Dept> {

}
