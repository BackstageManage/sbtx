package com.haifeng.sbtx.business.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.sbtx.business.RoleService;
import com.haifeng.sbtx.mapper.entity.Role;
import com.haifeng.sbtx.mapper.mapper.RoleMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
