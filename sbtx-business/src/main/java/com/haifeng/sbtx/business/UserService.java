package com.haifeng.sbtx.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haifeng.sbtx.mapper.entity.User;
import com.haifeng.sbtx.mapper.entity.dto.UserDto;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface UserService extends IService<User> {

    /**
     *  根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    UserDto getUserByUsername(String username);

}
