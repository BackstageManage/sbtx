package com.haifeng.sbtx.business.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.sbtx.business.DeptService;
import com.haifeng.sbtx.mapper.entity.Dept;
import com.haifeng.sbtx.mapper.mapper.DeptMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

}
