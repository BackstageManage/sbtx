package com.haifeng.sbtx.business.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.sbtx.business.PermissionService;
import com.haifeng.sbtx.mapper.entity.Permission;
import com.haifeng.sbtx.mapper.mapper.PermissionMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
