package com.haifeng.sbtx.business.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haifeng.sbtx.business.UserService;
import com.haifeng.sbtx.mapper.entity.User;
import com.haifeng.sbtx.mapper.entity.dto.UserDto;
import com.haifeng.sbtx.mapper.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     *  根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    @Override
    public UserDto getUserByUsername(String username) {
        return userMapper.getUserByUsername(username);
    }

}
