package com.haifeng.sbtx.api.controller;

import com.haifeng.sbtx.common.base.Rest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  登录认证
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-09
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    /**
     * 需要登录认证
     * @return
     */
    @GetMapping("/login")
    public Rest login (){
        return Rest.error(HttpStatus.UNAUTHORIZED.value(), "请先登录");
    }
}
