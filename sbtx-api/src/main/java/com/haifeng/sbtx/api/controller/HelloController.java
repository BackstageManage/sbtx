package com.haifeng.sbtx.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  Hello测试
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-12-09
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello 123";
    }

    @GetMapping("/noAuth")
    public String hello1(){
        return "hello 123";
    }
}
