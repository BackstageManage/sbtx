package com.haifeng.sbtx.api.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableSwaggerBootstrapUi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>
 *  Swagger配置类(只用于开发环境dev)
 * </p>
 *
 * @author: Haifeng
 * @date: 2019-11-28
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUi
@Profile("dev")
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("SBT接口文档 1.0")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.haifeng.sbt.api.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SBT接口文档")
                .description("SBT接口文档")
                .termsOfServiceUrl("http://localhost:8080/doc.html")
                .contact(new Contact("王海峰","","wanghaifeng2556@aliyun.com"))
                .version("1.0")
                .build();
    }
}
