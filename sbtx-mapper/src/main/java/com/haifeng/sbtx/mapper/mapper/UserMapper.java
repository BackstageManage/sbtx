package com.haifeng.sbtx.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haifeng.sbtx.mapper.entity.User;
import com.haifeng.sbtx.mapper.entity.dto.UserDto;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     *  根据用户名获取用户信息
     * @param username 用户名
     * @return
     */
    UserDto getUserByUsername(String username);
}
