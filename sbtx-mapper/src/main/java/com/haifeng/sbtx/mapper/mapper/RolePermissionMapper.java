package com.haifeng.sbtx.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haifeng.sbtx.mapper.entity.RolePermission;

/**
 * <p>
 * 角色权限关联表 Mapper 接口
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
