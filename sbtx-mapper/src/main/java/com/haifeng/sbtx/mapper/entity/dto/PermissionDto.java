package com.haifeng.sbtx.mapper.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author haifeng
 * @since 2019-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_permission")
@ApiModel(value="Permission对象", description="权限表")
public class PermissionDto implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "权限编号")
    @TableField("permission_code")
    private String permissionCode;

    @ApiModelProperty(value = "权限名称")
    @TableField("permission_name")
    private String permissionName;

    @ApiModelProperty(value = "权限url")
    @TableField("permission_url")
    private String permissionUrl;

    @ApiModelProperty(value = "权限在当前目录下的排序")
    @TableField("permission_sort")
    private Integer permissionSort;

    @ApiModelProperty(value = "权限类型：1-菜单；2-按钮")
    @TableField("permission_type")
    private Boolean permissionType;

    @ApiModelProperty(value = "父权限")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否可用：0-不可用；1-可用")
    private Boolean enabled;

    @ApiModelProperty(value = "创建人id")
    @TableField("create_id")
    private Long createId;

    @ApiModelProperty(value = "创建人名称")
    @TableField("create_name")
    private String createName;

    @ApiModelProperty(value = "创建人ip")
    @TableField("create_ip")
    private String createIp;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_datetime")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新人id")
    @TableField("update_id")
    private Long updateId;

    @ApiModelProperty(value = "更新人名称")
    @TableField("update_name")
    private String updateName;

    @ApiModelProperty(value = "更新人ip")
    @TableField("update_ip")
    private String updateIp;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_datetime")
    private LocalDateTime updateDatetime;


}
