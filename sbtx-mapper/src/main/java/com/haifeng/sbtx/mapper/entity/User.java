package com.haifeng.sbtx.mapper.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author haifeng
 * @since 2019-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_user")
@ApiModel(value="User对象", description="用户表")
public class User implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码，加密存储")
    private String password;

    @ApiModelProperty(value = "部门id")
    @TableField("dept_id")
    private Long deptId;

    @ApiModelProperty(value = "类型：0-超级管理员；1-其他")
    @TableField("user_type")
    private Boolean userType;

    @ApiModelProperty(value = "手机号")
    @TableField("mobile_phone")
    private String mobilePhone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "身份证号")
    @TableField("id_card_no")
    private String idCardNo;

    @ApiModelProperty(value = "账户是否过期：0-过期；1-未过期")
    @TableField("account_non_expired")
    private Boolean accountNonExpired;

    @ApiModelProperty(value = "密码是否过期：0-过期；1-未过期")
    @TableField("credentials_non_expired")
    private Boolean credentialsNonExpired;

    @ApiModelProperty(value = "是否锁定：0-锁定；1-未锁定")
    @TableField("account_non_locked")
    private Boolean accountNonLocked;

    @ApiModelProperty(value = "上次登录ip")
    @TableField("last_login_ip")
    private String lastLoginIp;

    @ApiModelProperty(value = "上次登录时间")
    @TableField("last_login_time")
    private LocalDateTime lastLoginTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "是否可用：0-不可用；1-可用")
    private Boolean enabled;

    @ApiModelProperty(value = "创建人id")
    @TableField("create_id")
    private Long createId;

    @ApiModelProperty(value = "创建人名称")
    @TableField("create_name")
    private String createName;

    @ApiModelProperty(value = "创建人ip")
    @TableField("create_ip")
    private String createIp;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_datetime")
    private LocalDateTime createDatetime;

    @ApiModelProperty(value = "更新人id")
    @TableField("update_id")
    private Long updateId;

    @ApiModelProperty(value = "更新人名称")
    @TableField("update_name")
    private String updateName;

    @ApiModelProperty(value = "更新人ip")
    @TableField("update_ip")
    private String updateIp;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_datetime")
    private LocalDateTime updateDatetime;


}
